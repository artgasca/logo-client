﻿namespace LOGOclient
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_conectar = new System.Windows.Forms.Button();
            this.txt_box_ip = new System.Windows.Forms.TextBox();
            this.txt_box_tsap_local = new System.Windows.Forms.TextBox();
            this.txt_box_tsap_remote = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_desconectar = new System.Windows.Forms.Button();
            this.txt_box_error = new System.Windows.Forms.TextBox();
            this.tabControl_connectionType = new System.Windows.Forms.TabControl();
            this.tabPage_tsap = new System.Windows.Forms.TabPage();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage_rakSlot = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox_main = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btn_refreshOutputs = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btn_Q11 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btn_Q10 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn_Q9 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btn_Q8 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btn_Q7 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_Q6 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_Q5 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_Q4 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Q3 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Q2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Q1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Q0 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_refreshInputs = new System.Windows.Forms.Button();
            this.cb_input8 = new System.Windows.Forms.CheckBox();
            this.cb_input7 = new System.Windows.Forms.CheckBox();
            this.cb_input6 = new System.Windows.Forms.CheckBox();
            this.cb_input5 = new System.Windows.Forms.CheckBox();
            this.cb_input4 = new System.Windows.Forms.CheckBox();
            this.cb_input3 = new System.Windows.Forms.CheckBox();
            this.cb_input2 = new System.Windows.Forms.CheckBox();
            this.cb_input1 = new System.Windows.Forms.CheckBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl_bytes = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txt_box_setPoint = new System.Windows.Forms.TextBox();
            this.lbl_sensorValue = new System.Windows.Forms.Label();
            this.btn_setPoint = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_UpdateSensor = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.cb_input9 = new System.Windows.Forms.CheckBox();
            this.cb_input10 = new System.Windows.Forms.CheckBox();
            this.cb_input11 = new System.Windows.Forms.CheckBox();
            this.cb_input12 = new System.Windows.Forms.CheckBox();
            this.cb_input13 = new System.Windows.Forms.CheckBox();
            this.cb_input14 = new System.Windows.Forms.CheckBox();
            this.cb_input15 = new System.Windows.Forms.CheckBox();
            this.cb_input16 = new System.Windows.Forms.CheckBox();
            this.tabControl_connectionType.SuspendLayout();
            this.tabPage_tsap.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox_main.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_conectar
            // 
            this.btn_conectar.Location = new System.Drawing.Point(31, 41);
            this.btn_conectar.Name = "btn_conectar";
            this.btn_conectar.Size = new System.Drawing.Size(88, 23);
            this.btn_conectar.TabIndex = 0;
            this.btn_conectar.Text = "Connect";
            this.btn_conectar.UseVisualStyleBackColor = true;
            this.btn_conectar.Click += new System.EventHandler(this.btn_conectar_Click);
            // 
            // txt_box_ip
            // 
            this.txt_box_ip.Location = new System.Drawing.Point(31, 16);
            this.txt_box_ip.Name = "txt_box_ip";
            this.txt_box_ip.Size = new System.Drawing.Size(88, 20);
            this.txt_box_ip.TabIndex = 1;
            this.txt_box_ip.Text = "192.168.1.15";
            // 
            // txt_box_tsap_local
            // 
            this.txt_box_tsap_local.Location = new System.Drawing.Point(3, 26);
            this.txt_box_tsap_local.Name = "txt_box_tsap_local";
            this.txt_box_tsap_local.Size = new System.Drawing.Size(32, 20);
            this.txt_box_tsap_local.TabIndex = 2;
            this.txt_box_tsap_local.Text = "01";
            // 
            // txt_box_tsap_remote
            // 
            this.txt_box_tsap_remote.Location = new System.Drawing.Point(99, 26);
            this.txt_box_tsap_remote.Name = "txt_box_tsap_remote";
            this.txt_box_tsap_remote.Size = new System.Drawing.Size(32, 20);
            this.txt_box_tsap_remote.TabIndex = 3;
            this.txt_box_tsap_remote.Text = "02";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "IP";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Local (hex)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(96, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "PLC (hex)";
            // 
            // btn_desconectar
            // 
            this.btn_desconectar.Enabled = false;
            this.btn_desconectar.Location = new System.Drawing.Point(31, 77);
            this.btn_desconectar.Name = "btn_desconectar";
            this.btn_desconectar.Size = new System.Drawing.Size(88, 23);
            this.btn_desconectar.TabIndex = 7;
            this.btn_desconectar.Text = "Disconnect";
            this.btn_desconectar.UseVisualStyleBackColor = true;
            this.btn_desconectar.Click += new System.EventHandler(this.btn_desconectar_Click);
            // 
            // txt_box_error
            // 
            this.txt_box_error.Location = new System.Drawing.Point(12, 485);
            this.txt_box_error.Name = "txt_box_error";
            this.txt_box_error.Size = new System.Drawing.Size(458, 20);
            this.txt_box_error.TabIndex = 8;
            // 
            // tabControl_connectionType
            // 
            this.tabControl_connectionType.Controls.Add(this.tabPage_tsap);
            this.tabControl_connectionType.Controls.Add(this.tabPage_rakSlot);
            this.tabControl_connectionType.Location = new System.Drawing.Point(125, 19);
            this.tabControl_connectionType.Name = "tabControl_connectionType";
            this.tabControl_connectionType.SelectedIndex = 0;
            this.tabControl_connectionType.Size = new System.Drawing.Size(184, 81);
            this.tabControl_connectionType.TabIndex = 10;
            // 
            // tabPage_tsap
            // 
            this.tabPage_tsap.Controls.Add(this.textBox2);
            this.tabPage_tsap.Controls.Add(this.textBox1);
            this.tabPage_tsap.Controls.Add(this.txt_box_tsap_local);
            this.tabPage_tsap.Controls.Add(this.label2);
            this.tabPage_tsap.Controls.Add(this.txt_box_tsap_remote);
            this.tabPage_tsap.Controls.Add(this.label3);
            this.tabPage_tsap.Location = new System.Drawing.Point(4, 22);
            this.tabPage_tsap.Name = "tabPage_tsap";
            this.tabPage_tsap.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_tsap.Size = new System.Drawing.Size(176, 55);
            this.tabPage_tsap.TabIndex = 0;
            this.tabPage_tsap.Text = "TSAP";
            this.tabPage_tsap.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(137, 26);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(32, 20);
            this.textBox2.TabIndex = 8;
            this.textBox2.Text = "00";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(41, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(32, 20);
            this.textBox1.TabIndex = 7;
            this.textBox1.Text = "00";
            // 
            // tabPage_rakSlot
            // 
            this.tabPage_rakSlot.Location = new System.Drawing.Point(4, 22);
            this.tabPage_rakSlot.Name = "tabPage_rakSlot";
            this.tabPage_rakSlot.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_rakSlot.Size = new System.Drawing.Size(176, 55);
            this.tabPage_rakSlot.TabIndex = 1;
            this.tabPage_rakSlot.Text = "Rack/Slot";
            this.tabPage_rakSlot.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl_connectionType);
            this.groupBox1.Controls.Add(this.btn_conectar);
            this.groupBox1.Controls.Add(this.txt_box_ip);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btn_desconectar);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(312, 105);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection";
            // 
            // groupBox_main
            // 
            this.groupBox_main.Controls.Add(this.tabControl1);
            this.groupBox_main.Enabled = false;
            this.groupBox_main.Location = new System.Drawing.Point(12, 133);
            this.groupBox_main.Name = "groupBox_main";
            this.groupBox_main.Size = new System.Drawing.Size(458, 297);
            this.groupBox_main.TabIndex = 12;
            this.groupBox_main.TabStop = false;
            this.groupBox_main.Text = "PLC";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(11, 19);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(430, 275);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btn_refreshOutputs);
            this.tabPage1.Controls.Add(this.panel12);
            this.tabPage1.Controls.Add(this.panel11);
            this.tabPage1.Controls.Add(this.panel10);
            this.tabPage1.Controls.Add(this.panel9);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Controls.Add(this.panel7);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(422, 249);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Outputs";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btn_refreshOutputs
            // 
            this.btn_refreshOutputs.Location = new System.Drawing.Point(170, 220);
            this.btn_refreshOutputs.Name = "btn_refreshOutputs";
            this.btn_refreshOutputs.Size = new System.Drawing.Size(75, 23);
            this.btn_refreshOutputs.TabIndex = 6;
            this.btn_refreshOutputs.Text = "Refresh";
            this.btn_refreshOutputs.UseVisualStyleBackColor = true;
            this.btn_refreshOutputs.Click += new System.EventHandler(this.btn_refreshOutputs_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.btn_Q11);
            this.panel12.Controls.Add(this.label15);
            this.panel12.Location = new System.Drawing.Point(319, 140);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(97, 61);
            this.panel12.TabIndex = 4;
            // 
            // btn_Q11
            // 
            this.btn_Q11.Location = new System.Drawing.Point(19, 33);
            this.btn_Q11.Name = "btn_Q11";
            this.btn_Q11.Size = new System.Drawing.Size(52, 23);
            this.btn_Q11.TabIndex = 1;
            this.btn_Q11.Text = "ON";
            this.btn_Q11.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(36, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 16);
            this.label15.TabIndex = 2;
            this.label15.Text = "Q11";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btn_Q10);
            this.panel11.Controls.Add(this.label14);
            this.panel11.Location = new System.Drawing.Point(213, 140);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(97, 61);
            this.panel11.TabIndex = 4;
            // 
            // btn_Q10
            // 
            this.btn_Q10.Location = new System.Drawing.Point(25, 33);
            this.btn_Q10.Name = "btn_Q10";
            this.btn_Q10.Size = new System.Drawing.Size(52, 23);
            this.btn_Q10.TabIndex = 1;
            this.btn_Q10.Text = "ON";
            this.btn_Q10.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(36, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 16);
            this.label14.TabIndex = 2;
            this.label14.Text = "Q10";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btn_Q9);
            this.panel10.Controls.Add(this.label13);
            this.panel10.Location = new System.Drawing.Point(110, 140);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(97, 61);
            this.panel10.TabIndex = 4;
            // 
            // btn_Q9
            // 
            this.btn_Q9.Location = new System.Drawing.Point(25, 33);
            this.btn_Q9.Name = "btn_Q9";
            this.btn_Q9.Size = new System.Drawing.Size(52, 23);
            this.btn_Q9.TabIndex = 1;
            this.btn_Q9.Text = "ON";
            this.btn_Q9.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(36, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 16);
            this.label13.TabIndex = 2;
            this.label13.Text = "Q9";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btn_Q8);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Location = new System.Drawing.Point(6, 140);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(97, 61);
            this.panel9.TabIndex = 4;
            // 
            // btn_Q8
            // 
            this.btn_Q8.Location = new System.Drawing.Point(22, 33);
            this.btn_Q8.Name = "btn_Q8";
            this.btn_Q8.Size = new System.Drawing.Size(52, 23);
            this.btn_Q8.TabIndex = 1;
            this.btn_Q8.Text = "ON";
            this.btn_Q8.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(36, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 16);
            this.label12.TabIndex = 2;
            this.label12.Text = "Q8";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btn_Q7);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Location = new System.Drawing.Point(316, 73);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(97, 61);
            this.panel8.TabIndex = 4;
            // 
            // btn_Q7
            // 
            this.btn_Q7.Location = new System.Drawing.Point(22, 33);
            this.btn_Q7.Name = "btn_Q7";
            this.btn_Q7.Size = new System.Drawing.Size(52, 23);
            this.btn_Q7.TabIndex = 1;
            this.btn_Q7.Text = "ON";
            this.btn_Q7.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(36, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "Q7";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btn_Q6);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Location = new System.Drawing.Point(213, 73);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(97, 61);
            this.panel7.TabIndex = 4;
            // 
            // btn_Q6
            // 
            this.btn_Q6.Location = new System.Drawing.Point(25, 33);
            this.btn_Q6.Name = "btn_Q6";
            this.btn_Q6.Size = new System.Drawing.Size(52, 23);
            this.btn_Q6.TabIndex = 1;
            this.btn_Q6.Text = "ON";
            this.btn_Q6.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(36, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 16);
            this.label10.TabIndex = 2;
            this.label10.Text = "Q6";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btn_Q5);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Location = new System.Drawing.Point(110, 73);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(97, 61);
            this.panel6.TabIndex = 5;
            // 
            // btn_Q5
            // 
            this.btn_Q5.Location = new System.Drawing.Point(25, 33);
            this.btn_Q5.Name = "btn_Q5";
            this.btn_Q5.Size = new System.Drawing.Size(52, 23);
            this.btn_Q5.TabIndex = 1;
            this.btn_Q5.Text = "ON";
            this.btn_Q5.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(36, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "Q5";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btn_Q4);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Location = new System.Drawing.Point(6, 73);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(97, 61);
            this.panel5.TabIndex = 4;
            // 
            // btn_Q4
            // 
            this.btn_Q4.Location = new System.Drawing.Point(22, 33);
            this.btn_Q4.Name = "btn_Q4";
            this.btn_Q4.Size = new System.Drawing.Size(52, 23);
            this.btn_Q4.TabIndex = 1;
            this.btn_Q4.Text = "ON";
            this.btn_Q4.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(36, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "Q4";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_Q3);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Location = new System.Drawing.Point(316, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(97, 61);
            this.panel4.TabIndex = 4;
            // 
            // btn_Q3
            // 
            this.btn_Q3.Location = new System.Drawing.Point(22, 33);
            this.btn_Q3.Name = "btn_Q3";
            this.btn_Q3.Size = new System.Drawing.Size(52, 23);
            this.btn_Q3.TabIndex = 1;
            this.btn_Q3.Text = "ON";
            this.btn_Q3.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(36, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "Q3";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_Q2);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Location = new System.Drawing.Point(213, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(97, 61);
            this.panel3.TabIndex = 4;
            // 
            // btn_Q2
            // 
            this.btn_Q2.Location = new System.Drawing.Point(25, 33);
            this.btn_Q2.Name = "btn_Q2";
            this.btn_Q2.Size = new System.Drawing.Size(52, 23);
            this.btn_Q2.TabIndex = 1;
            this.btn_Q2.Text = "ON";
            this.btn_Q2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "Q2";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_Q1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(110, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(97, 61);
            this.panel2.TabIndex = 3;
            // 
            // btn_Q1
            // 
            this.btn_Q1.Location = new System.Drawing.Point(25, 33);
            this.btn_Q1.Name = "btn_Q1";
            this.btn_Q1.Size = new System.Drawing.Size(52, 23);
            this.btn_Q1.TabIndex = 1;
            this.btn_Q1.Text = "ON";
            this.btn_Q1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "Q1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_Q0);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(97, 61);
            this.panel1.TabIndex = 2;
            // 
            // btn_Q0
            // 
            this.btn_Q0.Location = new System.Drawing.Point(22, 33);
            this.btn_Q0.Name = "btn_Q0";
            this.btn_Q0.Size = new System.Drawing.Size(52, 23);
            this.btn_Q0.TabIndex = 1;
            this.btn_Q0.Text = "ON";
            this.btn_Q0.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Q0";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_refreshInputs);
            this.tabPage2.Controls.Add(this.cb_input16);
            this.tabPage2.Controls.Add(this.cb_input15);
            this.tabPage2.Controls.Add(this.cb_input14);
            this.tabPage2.Controls.Add(this.cb_input13);
            this.tabPage2.Controls.Add(this.cb_input12);
            this.tabPage2.Controls.Add(this.cb_input11);
            this.tabPage2.Controls.Add(this.cb_input10);
            this.tabPage2.Controls.Add(this.cb_input9);
            this.tabPage2.Controls.Add(this.cb_input8);
            this.tabPage2.Controls.Add(this.cb_input7);
            this.tabPage2.Controls.Add(this.cb_input6);
            this.tabPage2.Controls.Add(this.cb_input5);
            this.tabPage2.Controls.Add(this.cb_input4);
            this.tabPage2.Controls.Add(this.cb_input3);
            this.tabPage2.Controls.Add(this.cb_input2);
            this.tabPage2.Controls.Add(this.cb_input1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(422, 249);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Inputs";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_refreshInputs
            // 
            this.btn_refreshInputs.Location = new System.Drawing.Point(170, 220);
            this.btn_refreshInputs.Name = "btn_refreshInputs";
            this.btn_refreshInputs.Size = new System.Drawing.Size(75, 23);
            this.btn_refreshInputs.TabIndex = 16;
            this.btn_refreshInputs.Text = "Refresh";
            this.btn_refreshInputs.UseVisualStyleBackColor = true;
            this.btn_refreshInputs.Click += new System.EventHandler(this.btn_refreshInputs_Click);
            // 
            // cb_input8
            // 
            this.cb_input8.AutoSize = true;
            this.cb_input8.Location = new System.Drawing.Point(334, 32);
            this.cb_input8.Name = "cb_input8";
            this.cb_input8.Size = new System.Drawing.Size(35, 17);
            this.cb_input8.TabIndex = 7;
            this.cb_input8.Text = "I8";
            this.cb_input8.UseVisualStyleBackColor = true;
            // 
            // cb_input7
            // 
            this.cb_input7.AutoSize = true;
            this.cb_input7.Location = new System.Drawing.Point(293, 32);
            this.cb_input7.Name = "cb_input7";
            this.cb_input7.Size = new System.Drawing.Size(35, 17);
            this.cb_input7.TabIndex = 6;
            this.cb_input7.Text = "I7";
            this.cb_input7.UseVisualStyleBackColor = true;
            // 
            // cb_input6
            // 
            this.cb_input6.AutoSize = true;
            this.cb_input6.Location = new System.Drawing.Point(252, 32);
            this.cb_input6.Name = "cb_input6";
            this.cb_input6.Size = new System.Drawing.Size(35, 17);
            this.cb_input6.TabIndex = 5;
            this.cb_input6.Text = "I6";
            this.cb_input6.UseVisualStyleBackColor = true;
            // 
            // cb_input5
            // 
            this.cb_input5.AutoSize = true;
            this.cb_input5.Location = new System.Drawing.Point(211, 32);
            this.cb_input5.Name = "cb_input5";
            this.cb_input5.Size = new System.Drawing.Size(35, 17);
            this.cb_input5.TabIndex = 4;
            this.cb_input5.Text = "I5";
            this.cb_input5.UseVisualStyleBackColor = true;
            // 
            // cb_input4
            // 
            this.cb_input4.AutoSize = true;
            this.cb_input4.Location = new System.Drawing.Point(170, 32);
            this.cb_input4.Name = "cb_input4";
            this.cb_input4.Size = new System.Drawing.Size(35, 17);
            this.cb_input4.TabIndex = 3;
            this.cb_input4.Text = "I4";
            this.cb_input4.UseVisualStyleBackColor = true;
            // 
            // cb_input3
            // 
            this.cb_input3.AutoSize = true;
            this.cb_input3.Location = new System.Drawing.Point(129, 32);
            this.cb_input3.Name = "cb_input3";
            this.cb_input3.Size = new System.Drawing.Size(35, 17);
            this.cb_input3.TabIndex = 2;
            this.cb_input3.Text = "I3";
            this.cb_input3.UseVisualStyleBackColor = true;
            // 
            // cb_input2
            // 
            this.cb_input2.AutoSize = true;
            this.cb_input2.Location = new System.Drawing.Point(88, 32);
            this.cb_input2.Name = "cb_input2";
            this.cb_input2.Size = new System.Drawing.Size(35, 17);
            this.cb_input2.TabIndex = 1;
            this.cb_input2.Text = "I2";
            this.cb_input2.UseVisualStyleBackColor = true;
            // 
            // cb_input1
            // 
            this.cb_input1.AutoSize = true;
            this.cb_input1.Location = new System.Drawing.Point(47, 32);
            this.cb_input1.Name = "cb_input1";
            this.cb_input1.Size = new System.Drawing.Size(35, 17);
            this.cb_input1.TabIndex = 0;
            this.cb_input1.Text = "I1";
            this.cb_input1.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(422, 249);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Demo";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(316, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(161, 124);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(344, 437);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Bytes Read";
            // 
            // lbl_bytes
            // 
            this.lbl_bytes.AutoSize = true;
            this.lbl_bytes.Location = new System.Drawing.Point(427, 437);
            this.lbl_bytes.Name = "lbl_bytes";
            this.lbl_bytes.Size = new System.Drawing.Size(13, 13);
            this.lbl_bytes.TabIndex = 15;
            this.lbl_bytes.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Value";
            // 
            // txt_box_setPoint
            // 
            this.txt_box_setPoint.Location = new System.Drawing.Point(6, 36);
            this.txt_box_setPoint.Name = "txt_box_setPoint";
            this.txt_box_setPoint.Size = new System.Drawing.Size(100, 20);
            this.txt_box_setPoint.TabIndex = 2;
            // 
            // lbl_sensorValue
            // 
            this.lbl_sensorValue.AutoSize = true;
            this.lbl_sensorValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sensorValue.Location = new System.Drawing.Point(117, 21);
            this.lbl_sensorValue.Name = "lbl_sensorValue";
            this.lbl_sensorValue.Size = new System.Drawing.Size(24, 24);
            this.lbl_sensorValue.TabIndex = 4;
            this.lbl_sensorValue.Text = "--";
            // 
            // btn_setPoint
            // 
            this.btn_setPoint.Location = new System.Drawing.Point(119, 36);
            this.btn_setPoint.Name = "btn_setPoint";
            this.btn_setPoint.Size = new System.Drawing.Size(75, 23);
            this.btn_setPoint.TabIndex = 6;
            this.btn_setPoint.Text = "Set";
            this.btn_setPoint.UseVisualStyleBackColor = true;
            this.btn_setPoint.Click += new System.EventHandler(this.btn_setPoint_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txt_box_setPoint);
            this.groupBox2.Controls.Add(this.btn_setPoint);
            this.groupBox2.Location = new System.Drawing.Point(11, 139);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(204, 79);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Set Point";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_UpdateSensor);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.lbl_sensorValue);
            this.groupBox3.Location = new System.Drawing.Point(11, 23);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(204, 89);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sensor";
            // 
            // btn_UpdateSensor
            // 
            this.btn_UpdateSensor.Location = new System.Drawing.Point(119, 60);
            this.btn_UpdateSensor.Name = "btn_UpdateSensor";
            this.btn_UpdateSensor.Size = new System.Drawing.Size(75, 23);
            this.btn_UpdateSensor.TabIndex = 10;
            this.btn_UpdateSensor.Text = "Update";
            this.btn_UpdateSensor.UseVisualStyleBackColor = true;
            this.btn_UpdateSensor.Click += new System.EventHandler(this.btn_UpdateSensor_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 61);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(46, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = "(1-1000)";
            // 
            // cb_input9
            // 
            this.cb_input9.AutoSize = true;
            this.cb_input9.Location = new System.Drawing.Point(47, 68);
            this.cb_input9.Name = "cb_input9";
            this.cb_input9.Size = new System.Drawing.Size(35, 17);
            this.cb_input9.TabIndex = 8;
            this.cb_input9.Text = "I9";
            this.cb_input9.UseVisualStyleBackColor = true;
            // 
            // cb_input10
            // 
            this.cb_input10.AutoSize = true;
            this.cb_input10.Location = new System.Drawing.Point(88, 68);
            this.cb_input10.Name = "cb_input10";
            this.cb_input10.Size = new System.Drawing.Size(41, 17);
            this.cb_input10.TabIndex = 9;
            this.cb_input10.Text = "I10";
            this.cb_input10.UseVisualStyleBackColor = true;
            // 
            // cb_input11
            // 
            this.cb_input11.AutoSize = true;
            this.cb_input11.Location = new System.Drawing.Point(129, 68);
            this.cb_input11.Name = "cb_input11";
            this.cb_input11.Size = new System.Drawing.Size(41, 17);
            this.cb_input11.TabIndex = 10;
            this.cb_input11.Text = "I11";
            this.cb_input11.UseVisualStyleBackColor = true;
            // 
            // cb_input12
            // 
            this.cb_input12.AutoSize = true;
            this.cb_input12.Location = new System.Drawing.Point(170, 68);
            this.cb_input12.Name = "cb_input12";
            this.cb_input12.Size = new System.Drawing.Size(41, 17);
            this.cb_input12.TabIndex = 11;
            this.cb_input12.Text = "I12";
            this.cb_input12.UseVisualStyleBackColor = true;
            // 
            // cb_input13
            // 
            this.cb_input13.AutoSize = true;
            this.cb_input13.Location = new System.Drawing.Point(211, 68);
            this.cb_input13.Name = "cb_input13";
            this.cb_input13.Size = new System.Drawing.Size(41, 17);
            this.cb_input13.TabIndex = 12;
            this.cb_input13.Text = "I13";
            this.cb_input13.UseVisualStyleBackColor = true;
            // 
            // cb_input14
            // 
            this.cb_input14.AutoSize = true;
            this.cb_input14.Location = new System.Drawing.Point(252, 68);
            this.cb_input14.Name = "cb_input14";
            this.cb_input14.Size = new System.Drawing.Size(41, 17);
            this.cb_input14.TabIndex = 13;
            this.cb_input14.Text = "I14";
            this.cb_input14.UseVisualStyleBackColor = true;
            // 
            // cb_input15
            // 
            this.cb_input15.AutoSize = true;
            this.cb_input15.Location = new System.Drawing.Point(293, 68);
            this.cb_input15.Name = "cb_input15";
            this.cb_input15.Size = new System.Drawing.Size(41, 17);
            this.cb_input15.TabIndex = 14;
            this.cb_input15.Text = "I15";
            this.cb_input15.UseVisualStyleBackColor = true;
            // 
            // cb_input16
            // 
            this.cb_input16.AutoSize = true;
            this.cb_input16.Location = new System.Drawing.Point(334, 68);
            this.cb_input16.Name = "cb_input16";
            this.cb_input16.Size = new System.Drawing.Size(41, 17);
            this.cb_input16.TabIndex = 15;
            this.cb_input16.Text = "I16";
            this.cb_input16.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 536);
            this.Controls.Add(this.lbl_bytes);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.groupBox_main);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_box_error);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "LOGO Client V1.0";
            this.tabControl_connectionType.ResumeLayout(false);
            this.tabPage_tsap.ResumeLayout(false);
            this.tabPage_tsap.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox_main.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_conectar;
        private System.Windows.Forms.TextBox txt_box_ip;
        private System.Windows.Forms.TextBox txt_box_tsap_local;
        private System.Windows.Forms.TextBox txt_box_tsap_remote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_desconectar;
        private System.Windows.Forms.TextBox txt_box_error;
        private System.Windows.Forms.TabControl tabControl_connectionType;
        private System.Windows.Forms.TabPage tabPage_tsap;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabPage tabPage_rakSlot;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox_main;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Q0;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btn_Q11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btn_Q10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btn_Q9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btn_Q8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btn_Q7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_Q6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_Q5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btn_Q4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_Q3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Q2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Q1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_refreshInputs;
        private System.Windows.Forms.CheckBox cb_input8;
        private System.Windows.Forms.CheckBox cb_input7;
        private System.Windows.Forms.CheckBox cb_input6;
        private System.Windows.Forms.CheckBox cb_input5;
        private System.Windows.Forms.CheckBox cb_input4;
        private System.Windows.Forms.CheckBox cb_input3;
        private System.Windows.Forms.CheckBox cb_input2;
        private System.Windows.Forms.CheckBox cb_input1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btn_refreshOutputs;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbl_bytes;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_UpdateSensor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbl_sensorValue;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_box_setPoint;
        private System.Windows.Forms.Button btn_setPoint;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox cb_input16;
        private System.Windows.Forms.CheckBox cb_input15;
        private System.Windows.Forms.CheckBox cb_input14;
        private System.Windows.Forms.CheckBox cb_input13;
        private System.Windows.Forms.CheckBox cb_input12;
        private System.Windows.Forms.CheckBox cb_input11;
        private System.Windows.Forms.CheckBox cb_input10;
        private System.Windows.Forms.CheckBox cb_input9;
    }
}

